import image from "../asset/img/devcampreact.png";

const gDevcampReact = {
    title: 'Chào mừng đến với Devcamp React',
    image,
    benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
    studyingStudents: 20,
    totalStudents: 100
  };
  
  const tinhTyLeSinhVien = ()=>gDevcampReact.studyingStudents / gDevcampReact.totalStudents * 100;

  export {gDevcampReact, tinhTyLeSinhVien};