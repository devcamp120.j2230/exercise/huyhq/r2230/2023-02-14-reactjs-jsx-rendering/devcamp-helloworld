import { gDevcampReact, tinhTyLeSinhVien } from "./data/info";

function App() {
  return (
    <div>
      <h1>{gDevcampReact.title}</h1>
      <img src={gDevcampReact.image} width="1000px"/>
      <p>Ty le sinh vien: {tinhTyLeSinhVien()}% 
          ({tinhTyLeSinhVien() > 15 ? "Sinh viên đăng ký học nhiều": "Sinh viên đăng ký học ít" })
      </p>
      <ul>
        {gDevcampReact.benefits.map((value, i)=>{
          return <li key={i}>{value}</li>
        })}
      </ul>
    </div>
  );
}

export default App;
